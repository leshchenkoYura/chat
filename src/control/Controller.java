package control;

import io.reactivex.disposables.CompositeDisposable;
import model.ChatIO;
import model.IChatIO;
import view.Frame;
import view.IPanel;
import view.Route;
import view.chat.ChatPanel;
import view.login.LoginPanel;
import java.io.IOException;

import static javax.swing.JOptionPane.*;

public class Controller implements IController {
    private CompositeDisposable compositeDisposable;
    private Route route;
    private IPanel panel;
    private IChatIO io;

    public Controller() {
        compositeDisposable = new CompositeDisposable();
        route = new Frame();
    }


    @Override
    public void login() {
        panel = new LoginPanel(this);
        route.viewLogin();
        route.addView(panel);
        route.setVisible(true);
    }

    @Override
    public void connect(String login) {
        try {
            io = new ChatIO(this);
            ((ChatIO) io).init();
            io.connect(login);
            response();
            chat();
        } catch (IOException e) {
            e.printStackTrace();
            refreshFrame();
        }

    }


    private void refreshFrame() {
        showMessageDialog(null, "Not connect Socket server");
        route.refresh();
    }


    private void chat() {
        route.remove(panel);
        panel = new ChatPanel(this);
        route.viewChat();
        route.addView(panel);
        route.setVisible(true);
    }

    @Override
    public void send(String message) {
        try {
            io.send(message);
        } catch (IOException e) {
            showMessageDialog(null, "Not connect Socket server");
            e.printStackTrace();
            route.refresh();
        }
    }

    @Override
    public void response() {
        compositeDisposable.add(io.response()
                .subscribe(v -> panel.response(v),
                        throwable -> disconnect()));
    }

    @Override
    public void disconnect() {
        if (io != null) {
            try {
                if(io.isConnected())
                io.disconnect();
                io = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        compositeDisposable.dispose();
        panel = null;
        route.dispose();

    }

}
