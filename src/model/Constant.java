package model;

public abstract class Constant {
    public static final String TITLE_LOGIN = "login";
    public static final String TITLE_CHAT = "chat";
    public static final String TAG_LOGIN = "login:";
    public static final String TAG_MESSAGE = "msg:";
    public static final String TAG_EXIT = "exit:";

}
