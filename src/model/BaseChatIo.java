package model;

import io.reactivex.Observable;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public abstract class BaseChatIo {
    protected Socket cs;
    protected DataOutputStream out;
    protected DataInputStream in;
    protected Observable<String> response;
    protected final String adress = "127.0.0.1";
    protected final int port = 7771;

    protected abstract void init() throws IOException;


    protected void writeUTF(String data) throws IOException {
        if(out == null){
            return;
        }
            out.writeUTF(data);
            out.flush();
    }

    protected void disconnect() throws IOException {
        if (cs != null && in != null && out != null) {
            try {
                cs.close();
                in.close();
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            cs = null;
            in = null;
            out = null;
        }
    }
}
