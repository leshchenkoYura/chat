package model;


import io.reactivex.Observable;

import java.io.IOException;

public interface IChatIO {
    void connect(String login) throws IOException;

    void send(String message) throws IOException;

    Observable<String> response();

    boolean isConnected();

    void disconnect() throws IOException;
}
