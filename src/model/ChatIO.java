package model;

import control.IController;
import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

public class ChatIO extends BaseChatIo implements IChatIO {
    private IController controller;

    public ChatIO(IController controller) {
        this.controller = controller;
    }

    @Override
    public void init() throws IOException {
        cs = new Socket(adress, port);
        out = new DataOutputStream(cs.getOutputStream());
        in = new DataInputStream(cs.getInputStream());
        response = Observable.interval(50, TimeUnit.MILLISECONDS, Schedulers.io())
                .flatMap(f -> Observable.just(in.available())
                        .doOnError(throwable -> disconnect())
                        .filter(v -> v > 0))
                .flatMap(v -> Observable.just(in.readUTF()));
    }

    @Override
    public void connect(String login) throws IOException {
        writeUTF(Constant.TAG_LOGIN.concat(login));

    }

    @Override
    public void send(String message) throws IOException {
//        if (!isConnected()) {
//            disconnect();
//        }
        writeUTF(Constant.TAG_MESSAGE.concat(message));
    }

    @Override
    public Observable<String> response() {
        return response.filter(v -> v != null && !v.isEmpty())
                .doOnError(Throwable::getMessage);
    }

    @Override
    public boolean isConnected() {
        try {
            return cs.isConnected();
        }catch (Exception e){
            return false;
        }
    }

    @Override
    public void disconnect() {
        try {
            if(isConnected()){
                writeUTF(Constant.TAG_EXIT);
            }
            super.disconnect();
            controller.disconnect();
        } catch (IOException e) {
            e.printStackTrace();
            try {
                super.disconnect();
                controller.disconnect();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }

    }
}
