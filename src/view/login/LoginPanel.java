package view.login;

import control.IController;
import view.IPanel;
import javax.swing.*;
import java.awt.*;


public class LoginPanel extends JPanel implements IPanel {
    private IController controller;
    private JLabel lbl;
    private JTextField txt;
    private JButton btnOk, btnCancel;

    public LoginPanel(IController controller) {
        this.controller = controller;
        setLayout(null);
        setBackground(Color.BLACK);
        view();
        listener();
    }

    private void view() {
        lbl = new JLabel("Enter Login");
        lbl.setBounds(100, 40, 200, 20);
        lbl.setForeground(Color.RED);

        txt = new JTextField();
        txt.setBounds(100, 60, 200, 20);

        btnOk = new JButton("Connect to Server");
        btnOk.setBounds(100, 90, 200, 20);
        btnOk.setBackground(Color.WHITE);


        btnCancel = new JButton("Cancel");
        btnCancel.setBounds(100, 110, 200, 20);
        btnCancel.setBackground(Color.WHITE);

        add(lbl);
        add(txt);
        add(btnOk);
        add(btnCancel);
    }

    private void listener() {
        btnCancel.addActionListener(e -> {
            controller.disconnect();
        });
        btnOk.addActionListener(e -> {
            controller.connect(txt.getText());
        });
    }

    @Override
    public void response(String message) {
        //TODO not used
    }
}
