package view.chat;

import control.IController;
import view.IPanel;

import javax.swing.*;
import java.awt.*;

public class ChatPanel extends JPanel implements IPanel {
    private IController controller;
    JTextArea txtArea = null;
    JTextField txt = null;
    JButton btnSend ,btnExit;
    JLabel lbl1, lbl2;




    public ChatPanel(IController controller) {
        this.controller = controller;
        setLayout(null);
        setBackground(Color.BLACK);
        view();
        listener();

    }
    
    private void view(){
        lbl1 = new JLabel("Chat Echo");
        lbl1.setBounds(10, 10, 770, 20);
        lbl1.setForeground(Color.LIGHT_GRAY);

        txtArea = new JTextArea();
        txtArea.setBounds(10, 30, 770, 350);
        txtArea.setBackground(Color.WHITE);

        lbl2 = new JLabel("Write your message in the box below");
        lbl2.setBounds(10, 400, 770, 20);
        lbl2.setForeground(Color.RED);

        txt = new JTextField();
        txt.setBounds(10, 440, 770, 30);


        btnSend = new JButton("Send");
        btnSend.setBounds(50, 500, 100, 30);
        btnSend.setBackground(Color.WHITE);


        btnExit = new JButton("Exit");
        btnExit.setBounds(600, 500, 100, 30);
        btnExit.setBackground(Color.WHITE);

        add(lbl1);
        add(lbl2);
        add(txtArea);
        add(txt);
        add(btnSend);
        add(btnExit);
    }
    
    private void listener(){
        btnSend.addActionListener(e -> {
            controller.send(txt.getText());
            txt.setText("");
        });

        btnExit.addActionListener(e -> controller.disconnect());
    }

    @Override
    public void response(String message) {
        txtArea.append(message.concat("\n"));
        txtArea.setCaretPosition(0);
    }
}
